#include "stdafx.h"
#include "CppUnitTest.h"
#include "../../../Inventory/Inventory/Inventory.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace InventoryTest
{		
	TEST_CLASS(UnitTest1)
	{
	private:
		string itemName = "Test";

	public:
		
		TEST_METHOD(AddItem_Test)
		{
			Inventory inventory;
			inventory.Add(itemName);
			inventory.Add(itemName);
			Assert::IsTrue(inventory.GetItemAmount(itemName) == 2);
		}

		TEST_METHOD(UseItem_Test_Valid)
		{
			Inventory* inventory = new Inventory();
			inventory->Add(itemName);
			inventory->UseItem("Test");
			Assert::IsTrue(true);
		}

		TEST_METHOD(UseItem_Test_InValid)
		{
			Inventory inventory;
			Assert::IsTrue(inventory.UseItem(itemName) == "");
		}
	};
}