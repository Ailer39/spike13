#include "stdafx.h"
#include "CppUnitTest.h"
#include "../../../spike7/Zorkish/Zorkish/Location.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Spike3Test
{		
	TEST_CLASS(GraphTest)
	{
	public:
		
		TEST_METHOD(LocationMove_Test_Valid)
		{
			Location* location = new Location(1, "Start", "Start location");
			Location* location2 = new Location(2, "Second", "Second Location");
			location->AddLocation(location2);
			Assert::IsTrue(location->GetLocation(Direction::North)->GetId() == 2);
		}

		TEST_METHOD(LocationMove_Test_Invalid)
		{
			Location* location = new Location(1, "Start", "Start location");
			Location* location2 = new Location(2, "Second", "Second Location");
			location->AddLocation(location2);
			Assert::IsNull(location->GetLocation(Direction::East));
		}
	};
}