#include "stdafx.h"
#include "CppUnitTest.h"
#include "../../../spike1/GridWorld/GridWorld/GameLogic.h"
#include "../../../spike1/GridWorld/GridWorld/Player.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Spike13
{		
	TEST_CLASS(GridWorldTest)
	{
	public:
		
		TEST_METHOD(Move_Test_Valid)
		{
			GameLogic* logic = new GameLogic();
			Player* p  = logic->GetPlayer();
			logic->MovePlayer('N');
			Assert::IsTrue(p->GetXPos() == 2 &&
				p->GetYPos() == 6);
		}

		TEST_METHOD(Move_Test_Wall)
		{
			GameLogic* logic = new GameLogic();
			Player* p = logic->GetPlayer();
			logic->MovePlayer('E');
			Assert::IsTrue(p->GetXPos() == 2 &&
				p->GetYPos() == 7);
		}
	};
}